FROM tomcat:9.0.1-jre8-alpine

ADD ./webapp /usr/local/tomcat/webapps/webapp

CMD ["catalina.sh", "run"]

EXPOSE 8888/tcp
